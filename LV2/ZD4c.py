import matplotlib.pyplot as plt
import numpy as np
import skimage.io
img = skimage.io.imread('tiger.png', as_gray=True)
plt.figure(1)



width, height = img.shape
img1 = np.zeros((height, width))
for j in range (0, width):
    img1[:,j] = img[j,:] 



plt.imshow(img1, cmap='gray', vmin=0, vmax=255)
plt.show()