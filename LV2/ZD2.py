#Simulirajte  100  bacanja  igraće kocke(kocka    s    brojevima    1    do    6) pomoću  for  petlje  i  funkcije numpy.random.randint. 
#Na zaslon ispišite koliko puta se pojavio pojedini broj. 
#Pomoću histograma prikažite rezultat ovih bacanja

import numpy as np
import matplotlib.pyplot as plt

cnt1 = 0
cnt2 = 0
cnt3 = 0
cnt4 = 0
cnt5 = 0
cnt6 = 0

for x in range (100):

    x = np.random.randint(1,7)

    print(x)
 
    if x == 1:
        
        cnt1 += 1
        
    elif x == 2:
        
        cnt2 += 1
        
    elif x == 3:
        
        cnt3 += 1
         
    elif x == 4:
        
        cnt4 += 1
      
    elif x == 5:
        
        cnt5 += 1
      
    elif x == 6:
        
        cnt6 += 1
       

print("1 se pojavio", cnt2, "puta")
print("3 se pojavio", cnt1, "puta")
print("2 se pojavio", cnt3, "puta")
print("4 se pojavio", cnt4, "puta")
print("5 se pojavio", cnt5, "puta")
print("6 se pojavio", cnt6, "puta")

plt.hist(x,bins = 6)
plt.show