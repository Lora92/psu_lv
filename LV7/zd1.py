import numpy as np
import os
from skimage.color.colorconv import rgba2rgb
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from skimage.transform import resize
from skimage import color
import matplotlib.image as mpimg
from tensorflow.python.keras.layers.core import Flatten
from tensorflow.python.keras.layers.pooling import MaxPooling2D
from tensorflow.python.module.module import _flatten_module

os.chdir('LV7')
# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

#fashion_mnist = keras.datasets.fashion_mnist

#(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()



# TODO: prikazi nekoliko slika iz train skupa

#plt.figure(figsize=(10,10))
#for i in range(25):
  #  plt.subplot(5,5,i+1)
   # plt.xticks([])
   # plt.yticks([])
    #plt.grid(False)
    #plt.imshow(train_images[i], cmap=plt.cm.binary)

#plt.show()

filename = 'test.png'

img = mpimg.imread(filename)
img = color.rgb2gray(rgba2rgb(img))
img = resize(img, (28, 28))

plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))
plt.show()



# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu

model = keras.Sequential(
    [
        keras.Input(shape = input_shape),
        
        layers.Conv2D(32, kernel_size = (3,3), activation="relu"),
        layers.MaxPooling2D(pool_size = (2,2)),
        layers.Conv2D(16, kernel_size = (3,3), activation="relu"),
        layers.MaxPooling2D(pool_size = (2,2)),
        layers.Flatten(),
        layers.Dense(30, activation= 'relu'),
        layers.Dense(10, activation= 'softmax'),
    ]
)

model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()

model.compile(loss='categorical_crossentropy',
optimizer='adam',
metrics=['accuracy'])

# TODO: provedi ucenje mreze

model.fit(x_train_s, y_train_s, epochs=15, batch_size=32)

# TODO: Prikazi test accuracy i matricu zabune

loss_and_metrics = model.evaluate(x_test_s, y_test_s, batch_size=128)
print(loss_and_metrics)

# TODO: spremi model
model.save('model_cnn')
